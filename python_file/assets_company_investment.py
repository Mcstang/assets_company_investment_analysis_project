#!/usr/bin/env python
# coding: utf-8

# # Assets Company Investment 

# ## Data Cleaning

# In[1]:


# import the libraries
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import seaborn as sns

# reading data files
# using encoding = "ISO-8859-1" to avoid pandas encoding error
rounds = pd.read_csv("rounds2.csv", encoding = "ISO-8859-1")
companies = pd.read_csv("companies.txt", sep="\t", encoding = "ISO-8859-1")


# In[2]:


# rounds head
print(rounds.head())


# In[3]:


print(rounds.info(), "\n")
print(rounds.shape)


# In[4]:


# companies head
companies.head()


# In[5]:


companies.info()


# In[6]:


# identify the unique number of permalinks in companies
len(companies.permalink.unique())


# In[7]:


# converting all permalinks to lowercase
companies['permalink'] = companies['permalink'].str.lower()
companies.head()


# In[8]:


# look at unique values again
len(companies.permalink.unique())


# In[9]:


# check the unique company names in round file
len(rounds.company_permalink.unique())

# company_permalink is the column name in the round file


# In[10]:


# we need 66768 company_permalinks
# so we get the all words to lower case
# converting column to lowercase
rounds['company_permalink'] = rounds['company_permalink'].str.lower()
rounds.head()


# In[11]:


len(rounds.company_permalink.unique())


# In[12]:


# companies present in rounds file but not in (~) companies file
rounds.loc[~rounds['company_permalink'].isin(companies['permalink']), :]


# In[13]:


# looking at the indices with weird characters
rounds_original = pd.read_csv("rounds2.csv", encoding = "ISO-8859-1")
rounds_original.iloc[[29597, 31863, 45176, 58473], :]


# In[14]:


# we have some issues with the encode which we used for rounds data

import chardet

rawdata = open('rounds2.csv', 'rb').read()
result = chardet.detect(rawdata)
charenc = result['encoding']
print(charenc)


# In[15]:


print(result)


# In[16]:


rounds['company_permalink'] = rounds.company_permalink.str.encode('utf-8').str.decode('ascii', 'ignore')
rounds.loc[~rounds['company_permalink'].isin(companies['permalink']), :]


# In[17]:


# Look at unique values again
len(rounds.company_permalink.unique())


# In[18]:


# remove encoding from companies df
companies['permalink'] = companies.permalink.str.encode('utf-8').str.decode('ascii', 'ignore')


# In[19]:


# companies present in companies df but not in rounds df
companies.loc[~companies['permalink'].isin(rounds['company_permalink']), :]


# In[20]:


# write rounds file
rounds.to_csv("rounds_clean.csv", sep=',', index=False)

# write companies file
companies.to_csv("companies_clean.csv", sep='\t', index=False)


# In[21]:


# read the new, decoded csv files
rounds = pd.read_csv("rounds_clean.csv", encoding = "ISO-8859-1")
companies = pd.read_csv("companies_clean.csv", sep="\t", encoding = "ISO-8859-1")


# ## Fix the Missing Values

# In[22]:


# missing values in companies 
companies.isnull().sum()


# In[23]:


# missing values in rounds 
rounds.isnull().sum()


# In[24]:


# merging 
master = pd.merge(companies, rounds, how="inner", left_on="permalink", right_on="company_permalink")
master.head()


# In[25]:


# print column names
master.columns


# In[26]:


# removing redundant columns
master =  master.drop(['company_permalink'], axis=1) 


# In[27]:


# after dropping
master.columns


# In[28]:


# column-wise missing values 
master.isnull().sum()


# In[29]:


# summing up the missing values (column-wise) and displaying fraction of NaNs
round(100*(master.isnull().sum()/len(master.index)), 2)


# In[30]:


# dropping columns 
master = master.drop(['funding_round_code', 'homepage_url', 'founded_at', 'state_code', 'region', 'city'], axis=1)
master.head()


# In[31]:


# summing up the missing values (column-wise) and displaying fraction of NaNs again
round(100*(master.isnull().sum()/len(master.index)), 2)


# In[32]:


# summary stats of raised_amount_usd
master['raised_amount_usd'].describe()


# In[33]:


# removing NaNs in raised_amount_usd
master = master[~np.isnan(master['raised_amount_usd'])]
round(100*(master.isnull().sum()/len(master.index)), 2)


# In[34]:


country_codes = master['country_code'].astype('category')

# displaying frequencies of each category
country_codes.value_counts()


# In[35]:


# viewing fractions of counts of country_codes
100*(master['country_code'].value_counts()/len(master.index))


# In[36]:


# removing rows with missing country_codes
master = master[~pd.isnull(master['country_code'])]

# look at missing values
round(100*(master.isnull().sum()/len(master.index)), 2)


# In[37]:


# removing rows with missing category_list values
master = master[~pd.isnull(master['category_list'])]

# look at missing values
round(100*(master.isnull().sum()/len(master.index)), 2)


# In[38]:


# writing the clean dataframe to an another file
master.to_csv("master_df.csv", sep=',', index=False)


# In[39]:


# look at the master df info for number of rows etc.
master.info()


# In[40]:


# check the after missing value fixing observations
100*(len(master.index) / len(rounds.index))


# ## Analysis

# ### Funding Type Analysis

# In[41]:


df = pd.read_csv("master_df.csv", sep=",", encoding="ISO-8859-1")
df.head()


# In[42]:


# first, let's filter the df so it only contains the four specified funding types
df = df[(df.funding_round_type == "venture") | 
        (df.funding_round_type == "angel") | 
        (df.funding_round_type == "seed") | 
        (df.funding_round_type == "private_equity") ]


# In[43]:


# distribution of raised_amount_usd
sns.boxplot(y=df['raised_amount_usd'])
plt.yscale('log')
plt.show()


# In[44]:


# summary metrics
df['raised_amount_usd'].describe()


# In[45]:


# comparing summary stats across four categories
sns.boxplot(x='funding_round_type', y='raised_amount_usd', data=df)
plt.yscale('log')
plt.show()


# In[46]:


# compare the mean and median values across categories
df.pivot_table(values='raised_amount_usd', columns='funding_round_type', aggfunc=[np.median, np.mean])


# In[47]:


# compare the median investment amount across the types
df.groupby('funding_round_type')['raised_amount_usd'].median().sort_values(ascending=False)


# ### Country analysis

# In[48]:


# filter the df for private equity type investments
df = df[df.funding_round_type=="venture"]

# group by country codes and compare the total funding amounts
country_wise_total = df.groupby('country_code')['raised_amount_usd'].sum().sort_values(ascending=False)
print(country_wise_total)


# In[49]:


# top 9 countries
top_9_countries = country_wise_total[:9]
top_9_countries


# In[50]:


# filtering for the top three countries
df = df[(df.country_code=='USA') | (df.country_code=='GBR') | (df.country_code=='IND')]
df.head()


# In[51]:


# filtered df has about 38800 observations
df.info()


# In[52]:


# boxplot to see distributions of funding amount across countries
plt.figure(figsize=(10, 10))
sns.boxplot(x='country_code', y='raised_amount_usd', data=df)
plt.yscale('log')
plt.show()


# ### Sector Analysis

# In[53]:


# extracting the main category
df.loc[:, 'main_category'] = df['category_list'].apply(lambda x: x.split("|")[0])
df.head()


# In[54]:


# drop the category_list column
df = df.drop('category_list', axis=1)
df.head()


# In[55]:


# read mapping file
mapping = pd.read_csv("mapping.csv", sep=",")
mapping.head()


# In[56]:


# missing values in mapping file
mapping.isnull().sum()


# In[57]:


# remove the row with missing values
mapping = mapping[~pd.isnull(mapping['category_list'])]
mapping.isnull().sum()


# In[58]:


# converting common columns to lowercase
mapping['category_list'] = mapping['category_list'].str.lower()
df['main_category'] = df['main_category'].str.lower()


# In[59]:


print(df.head())


# In[60]:


mapping['category_list']


# In[61]:


# values in main_category column in df which are not in the category_list column in mapping file
df[~df['main_category'].isin(mapping['category_list'])]


# In[62]:


# values in the category_list column which are not in main_category column 
mapping[~mapping['category_list'].isin(df['main_category'])]


# In[63]:


# replacing '0' with 'na'
mapping['category_list'] = mapping['category_list'].apply(lambda x: x.replace('0', 'na'))
print(mapping['category_list'])


# In[64]:


# merge the dfs
df = pd.merge(df, mapping, how='inner', left_on='main_category', right_on='category_list')
df.head()


# In[65]:


# let's drop the category_list column since it is the same as main_category
df = df.drop('category_list', axis=1)
df.head()


# In[66]:


# look at the column types and names
df.info()


# #### Converting "wide" df to "long"

# In[67]:


# store the value and id variables in two separate arrays

# store the value variables in one Series
value_vars = df.columns[9:18]

# take the setdiff() to get the rest of the variables
id_vars = np.setdiff1d(df.columns, value_vars)

print(value_vars, "\n")
print(id_vars)


# In[68]:


# convert into long
long_df = pd.melt(df, 
        id_vars=list(id_vars), 
        value_vars=list(value_vars))

long_df.head()


# In[69]:


# remove rows having value=0
long_df = long_df[long_df['value']==1]
long_df = long_df.drop('value', axis=1)


# In[70]:


# look at the new df
long_df.head()
len(long_df)


# In[71]:


# renaming the 'variable' column
long_df = long_df.rename(columns={'variable': 'sector'})


# In[72]:


# info
long_df.info()


# In[73]:


# summarising the sector-wise number and sum of venture investments across three countries

# first, let's also filter for investment range between 5 and 15m
df = long_df[(long_df['raised_amount_usd'] >= 5000000) & (long_df['raised_amount_usd'] <= 15000000)]


# In[74]:


# groupby country, sector and compute the count and sum
df.groupby(['country_code', 'sector']).raised_amount_usd.agg(['count', 'sum'])


# In[75]:


# plotting sector-wise count and sum of investments in the three countries
plt.figure(figsize=(16, 14))

plt.subplot(2, 1, 1)
p = sns.barplot(x='sector', y='raised_amount_usd', hue='country_code', data=df, estimator=np.sum)
p.set_xticklabels(p.get_xticklabels(),rotation=30)
plt.title('Total Invested Amount (USD)')

plt.subplot(2, 1, 2)
q = sns.countplot(x='sector', hue='country_code', data=df)
q.set_xticklabels(q.get_xticklabels(),rotation=30)
plt.title('Number of Investments')


plt.show()


# In[ ]:




